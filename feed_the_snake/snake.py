import pygame
import random

screen_width = 800
screen_height = 600
X,Y = 0,1
UP,DOWN = -1,1
LEFT,RIGHT = -1,1

def init_screen():
  def setup_object():
    """ Create star and store in the star list """
    # Setup star
    #yellow +10
    for i in range(-1,100):
        new_rect = ystar_rect.copy()
        new_rect.topleft = (ycountrandomx[i] , ycountrandomy[i])
        ystar_positions.append(new_rect)
    #blue +5
    for i in range(-1,100):
        new_rect = bstar_rect.copy()
        new_rect.topleft = (bcountrandomx[i] , pcountrandomy[i])
        bstar_positions.append(new_rect)    
    #orange +20
    for i in range(-1,50):
        new_rect = ostar_rect.copy()
        new_rect.topleft = (ocountrandomx[i] , wcountrandomy[i])
        ostar_positions.append(new_rect)    
    #red -15
    for i in range(-1,75):
        new_rect = rstar_rect.copy()
        new_rect.topleft = (rcountrandomx[i] , rcountrandomy[i])
        rstar_positions.append(new_rect)    
    #setup upwall
    for i in range(0,17):
        new_wall = wall_rect.copy()
        new_wall.topleft = (i*50,0 )
        wall_positions.append(new_wall)
    #setup downwall    
    for i in range(0,17):
        new_wall = wall_rect.copy()
        new_wall.topleft = (i*50,screen_height-wall_rect.height)
        wall_positions.append(new_wall)    
    #setup trap 
    for i in range(1,4):
        new_trap = trap_rect.copy()
        if i == 2:
          new_trap.topleft = (300, 200)
        elif i == 3:
          new_trap.topleft = (600, 200)
        
        
        trap_positions.append(new_trap)       
   
  def event_handler(event):
    """ handling the occured events """
    if event.type == pygame.QUIT:  # click close button at top corner of the screen
      pygame.quit()
      
    elif event.type == pygame.KEYDOWN:
      if event.key == pygame.K_UP and snake_direction[Y] == 0: # occur when up key is pressed
        snake_direction[X] = 0
        snake_direction[Y] = UP
      elif event.key == pygame.K_DOWN and snake_direction[Y] == 0: # occur when down key is pressed
        snake_direction[X] = 0
        snake_direction[Y] = DOWN  
      elif event.key == pygame.K_LEFT and snake_direction[X] == 0: # occur when left key is pressed
        snake_direction[X] = LEFT
        snake_direction[Y] = 0  
      elif event.key == pygame.K_RIGHT and snake_direction[X] == 0: # occur when right key is pressed
        snake_direction[X] = RIGHT
        snake_direction[Y] = 0       
      elif event.key == pygame.K_F2:
        pygame.quit()  
      
  def render():
    """ render all the game components """
    screen.blit(background, (0,0)) 
    screen.blit(ystar_surface, ystar_positions[-1])  
    screen.blit(bstar_surface, bstar_positions[-1])  
    screen.blit(ostar_surface, ostar_positions[-1]) 
    screen.blit(rstar_surface, rstar_positions[-1])      
    for position in snake_body_positions: # iterare over the list and renser each snake part
      screen.blit(snake_surface, position)
    for position in wall_positions:
      screen.blit(wall_surface, position)
    for position in trap_positions:
      screen.blit(trap_surface, position)
     
     # display text score 
    font = pygame.font.Font(None,48)
    txtscore = font.render(str(playScore),1, (255,255,255))
    txtscore1= font.render(str("Your score is :"),1, (255,255,255))
    screen.blit (txtscore,(700,5))
    screen.blit (txtscore1,(500,5))
    
    
    #display snake's speed
    
    font1 = pygame.font.Font(None,42)
    txtspeed = font1.render(str(speed),1, (255,255,255))
    txtspeed1 = font1.render(str("speed x "),1, (255,255,255))
    screen.blit (txtspeed,(105,7))   
    screen.blit (txtspeed1,(10,7))
    
    pygame.display.flip()

  def check_collision():
    """ Detect all the collision, you may check out of the boundary here """
    nonlocal countERR,snake_body_positions,snake_direction,wall_positions,playScore
    eatystar = snake_body_positions[0].colliderect(ystar_positions[-1])
    eatbstar = snake_body_positions[0].colliderect(bstar_positions[-1])
    eatostar = snake_body_positions[0].colliderect(ostar_positions[-1])
    eatrstar = snake_body_positions[0].colliderect(rstar_positions[-1])
    crashwall = snake_body_positions[0].collidelist(wall_positions)
    crashtrap = snake_body_positions[0].collidelist(trap_positions)
    count = 0
    
    for i in snake_body_positions:
      if count == 0 :
        pass
      else:
        selfdeath = snake_body_positions[0].colliderect(i)
      count +=1
    
    if selfdeath >=1:
      newpositions = snake_body_positions
      snake_body_positions=[]
      countERR +=1
      if countERR < 3:
          failed_sound.play()
      snake_direction = [RIGHT,0]
      
            
      for i in newpositions:
        snake_body_positions.append(i)
      for i in range(len(snake_body_positions)):
        snake_body_positions[i].x = 400-40*i
        snake_body_positions[i].y = 300
        
    if crashwall >=0 :
        newpositions = snake_body_positions
        snake_body_positions=[]
        countERR += 1
        if countERR < 3:
          failed_sound.play()
        snake_direction = [RIGHT,0]
        for i in newpositions:
          snake_body_positions.append(i)
        for i in range(len(snake_body_positions)):
          snake_body_positions[i].x = 400-40*i
          snake_body_positions[i].y = 300  
      
    if crashtrap >=0 :
        newpositions = snake_body_positions
        snake_body_positions=[]
        countERR += 1
        if countERR < 3:
          failed_sound.play()
        snake_direction = [RIGHT,0]
        for i in newpositions:
            snake_body_positions.append(i)
        for i in range(len(snake_body_positions)):
            snake_body_positions[i].x = 400-40*i
            snake_body_positions[i].y = 300         
    
    if eatystar > 0:
    
      snaketail = snake_body_positions[-1]
      pos = snaketail.move(snaketail.x - snake_rect.width, 300)
      snake_body_positions.append(pos)
      if len(ystar_positions) > 1:
        eat_sound.play()
        ystar_positions.pop()
        playScore +=10
      
    if eatbstar > 0:
    
      snaketail = snake_body_positions[-1]
      pos = snaketail.move(snaketail.x - snake_rect.width, 300)
      snake_body_positions.append(pos)
      if len(bstar_positions) > 1:
        eat_sound.play()
        bstar_positions.pop()
        playScore +=5   
    
    if eatwfruit > 0:
   
      snaketail = snake_body_positions[-1]
      pos = snaketail.move(snaketail.x - snake_rect.width, 300)
      snake_body_positions.append(pos)
      if len(ostar_positions) > 1:
        
          eat_sound.play()
          ostar_positions.pop()
          playScore +=20
            
    if eatrstar > 0:
      snaketail = snake_body_positions[-1]
      pos = snaketail.move(snaketail.x - snake_rect.width, 300)
      snake_body_positions.append(pos)
      if len(ystar_positions) > 0:
        if len(snake_body_positions) < 3:
          snake_body_positions.pop()
        fail_sound.play()
        rstar_positions.pop()
        playScore -=15 

  def move():
    tail = snake_body_positions.pop()
    width = tail.width
    x = snake_body_positions[0].x + (width * snake_direction[X])
    y = snake_body_positions[0].y + (width * snake_direction[Y])
    tail.topleft = (x, y)
    snake_body_positions.insert(0, tail)
    #snake flip in new side (up,down)
    if snake_body_positions[0].x > screen_width:
      snake_body_positions[0].x = 0
    elif snake_body_positions[0].x < 0:
      snake_body_positions[0].x = screen_width 
      
  def update():
    """ Update value of the game components """
    move()
    check_collision() 
    
  
  # Initialization
  pygame.init()
  screen = pygame.display.set_mode((screen_width, screen_height))
  eat_sound = pygame.mixer.Sound('eat.wav')
  over_sound = pygame.mixer.Sound('gameover.wav')
  failed_sound = pygame.mixer.Sound('failed.wav')
  fail_sound = pygame.mixer.Sound('fail.wav')
  win_sound = pygame.mixer.Sound('win.wav')
  pygame.display.set_caption("Snake")
  ystar_surface = pygame.image.load("Green.png").convert_alpha()
  bstar_surface = pygame.image.load("blue.png").convert_alpha()
  ostar_surface = pygame.image.load("orange.png").convert_alpha()
  rstar_surface = pygame.image.load("red.png").convert_alpha()
  wall_surface = pygame.image.load("wall.png").convert_alpha()
  trap_surface = pygame.image.load("trap.png").convert_alpha()
  snake_surface = pygame.image.load("snake.png").convert_alpha()
 
  
  
  background = pygame.image.load("background.png").convert()
  gameover = pygame.image.load("Gameover.jpg").convert_alpha()
  suscess  = pygame.image.load("sucess.png").convert_alpha()
  ystar_rect = ystar_surface.get_rect()
  bstar_rect = bstar_surface.get_rect()
  rstar_rect = rstar_surface.get_rect()
  ostar_rect = ostar_surface.get_rect()
  wall_rect = wall_surface.get_rect()
  trap_rect = trap_surface.get_rect()
  snake_rect = snake_surface.get_rect()
  clock = pygame.time.Clock()
  snake_direction = [RIGHT,0]
  pos1 = snake_rect.move(400, 300)
  pos2 = snake_rect.move(pos1.x - snake_rect.width, 300)
  pos3 = pos2.move(pos2.x - snake_rect.width, 300)  
  snake_body_positions = [pos1, pos2, pos3]
  countERR = 0
  playScore = 0
  wall_positions = []
  trap_positions = []
  ystar_positions = []
  bstar_positions = []
  rstar_positions = []
  ostar_positions = []
  gameOver = False
  
  ycountrandomx= []
  ycountrandomy= []
  for i in range (0,200):
    ystartposx, ystopposx = 50, 750
    ystartposy, ystopposy = 50, 550
    yrandomx = random.randint(ystartposx, ystopposx)
    yrandomy = random.randint(ystartposy, ystopposy)
    ycountrandomx.append(yrandomx)
    ycountrandomy.append(yrandomy)
   
  bcountrandomx= []
  bcountrandomy= []
  for i in range (0,200):
    bstartposx, pstopposx = 50, 750
    bstartposy, pstopposy = 50, 550
    brandomx = random.randint(pstartposx, pstopposx)
    brandomy = random.randint(pstartposy, pstopposy)
    bcountrandomx.append(prandomx)
    bcountrandomy.append(prandomy)
   
  ocountrandomx= []
  ocountrandomy= []
  for i in range (0,200):
    ostartposx, wstopposx = 50, 750
    ostartposy, wstopposy = 50, 550
    orandomx = random.randint(wstartposx, wstopposx)
    orandomy = random.randint(wstartposy, wstopposy)
    ocountrandomx.append(wrandomx)
    ocountrandomy.append(wrandomy)
    
  rcountrandomx= []
  rcountrandomy= []
  for i in range (0,200):
    rstartposx, rstopposx = 50, 750
    rstartposy, rstopposy = 50, 550
    rrandomx = random.randint(rstartposx, rstopposx)
    rrandomy = random.randint(rstartposy, rstopposy)
    rcountrandomx.append(rrandomx)
    rcountrandomy.append(rrandomy) 
  setup_object()
  
  # Main game loop
  while not gameOver:
      if len(snake_body_positions) < 5:
        clock.tick(3)
        speed = 1
      elif len(snake_body_positions) < 10:
        clock.tick(4)
        speed = 2
      elif len(snake_body_positions) < 15:
        clock.tick(4) 
        speed = 3
      elif len(snake_body_positions) < 20:
        clock.tick(6) 
        speed = 4
      elif len(snake_body_positions) < 25:
        clock.tick(7) 
        speed = 5
      elif len(snake_body_positions) < 30:
        clock.tick(8) 
        speed = 6    
      else : 
        clock.tick(9)
        speed = 7
      for event in pygame.event.get():
        event_handler(event)
      update()
      render()           
      if countERR == 3 or playScore > 600 :
        gameOver = True 
      
      
  #gameover
  if countERR == 3:
    over_sound.play()
    screen.blit(gameover, (0,0))
  elif playScore > 600 :
    win_sound.play()
    screen.blit(suscess, (0,0))
  fonty = pygame.font.Font(None,72)
  txt2 = fonty.render(str(playScore),1, (255,255,255))
  screen.blit (txt2,(500,410))  
  pygame.display.flip()
  
  
# All the processes start here

init_screen()

#Start Game Again
while True:
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      pygame.quit()
    elif event.type == pygame.KEYDOWN :
      if event.key == pygame.K_F1:
        screen_width = 800
        screen_height = 600
        X,Y = 0,1
        UP,DOWN = -1,1
        LEFT,RIGHT = -1,1  
        score = 0
        init_screen()
      elif event.key == pygame.K_F2:
        pygame.quit()
        
      
